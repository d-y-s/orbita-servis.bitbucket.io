$('.js-machinery-slider').owlCarousel({
    items: 3,
    loop: true,
    nav: true,
    dots: false,
    autoplay: true,
    margin: 20,
    onInitialized: startProgressBarMachinery,
    onTranslate: resetProgressBarMachinery,
    onTranslated: startProgressBarMachinery,
    responsive: {
        0: {
            items: 1,
        },
        960: {
            items: 2,
        },
        1280: {
            items: 3,
        },
    },
    navText: ['<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/></g></svg>', '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/></g></svg>']
});


function startProgressBarMachinery(event) {

  $(".js-progress-bar-machinery").css({
    width: "100%",
    transition: "width 4000ms"
  });
  if (!event.namespace) {
      return;
    }
    var slides = event.relatedTarget;
    $('.machinery__counter').html(slides.relative(slides.current()) + 1 + '<span>' + ' /' + slides.items().length + '</span>');
}

function resetProgressBarMachinery() {
  $(".js-progress-bar-machinery").css({
    width: 0,
    transition: "width 0s"
  });
}