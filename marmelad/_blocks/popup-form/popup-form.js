$('.js-popup-form').on('submit', function(event) {
    event.preventDefault();
    $('.popup-form__notification').slideDown();
    $('.js-popup-form').slideUp();
});