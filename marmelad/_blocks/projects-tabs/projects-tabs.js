$('.projects-tabs__items').on('click', '.js-tab-toggle:not(.tab-toggle-is-active)', function() {
    $(this).addClass('tab-toggle-is-active').siblings().removeClass('tab-toggle-is-active');
    
    $('.projects-tabs__main').find('.js-tab-content').removeClass('tab-content-is-active').hide().eq($(this).index()).fadeIn();

    if (window.matchMedia("(max-width: 1279px)").matches) {
 
        var scrTop = $(document).scrollTop(),
            offsetTop = $(this).offset().top + 300;
        if (scrTop < offsetTop)
        {
            $('html, body').animate(
            {
                scrollTop: offsetTop
            }, 500);
        }
    }

});