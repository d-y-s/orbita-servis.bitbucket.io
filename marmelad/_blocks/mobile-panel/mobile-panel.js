$('.js-btn-burger').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('btn-burger-is-active');

    $('.mobile-panel .menu-mob').toggleClass('menu-is-active');
    $('.mobile-panel__overlay-menu').toggleClass('overlay-is-active');
});


$.each($('.menu-mob .main-menu__nav ul').find('> li'), function (index, element) {

    if ($(element).find(' > ul').length) {

        var triggerIcon = ['<div class="svg-icon svg-icon--angle-down">', '</div>'].join('');

        var subMenuTrigger = $('<div class="sub-menu-trigger">' + triggerIcon + '</div>');

        $(element)
            .addClass('haschild')
            .append(subMenuTrigger);
    }
});

$('.menu-mob .main-menu__nav ul .sub-menu-trigger').on('click', function (event) {
    $(this).toggleClass('rotade');
    if (!$(this).closest('li').find('>ul').length) {
        return;
    }
    event.preventDefault();
    $(this).closest('li').toggleClass('open').find('>ul').stop().slideToggle();
});	

function btnPhone() {

    $('.js-btn-phone').on('click', function () {
        $('.js-contacts-body').fadeToggle();
    });

    $(document).on('click', function (e) {
        if ($(e.target).closest('.mobile-panel').length) {
            return;
        }

        $('.js-contacts-body').fadeOut();
    });

}

btnPhone();