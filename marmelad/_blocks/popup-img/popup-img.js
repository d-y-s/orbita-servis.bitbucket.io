$('.js-img-viewing').on('click', function (event) {
    event.preventDefault();
    var $this = $(this),
        imgSrc = $this.find('img').clone(),
        inRemodalImg = $this.closest('.app').find('.popup-img');
        imgSrc.appendTo('.js-img-zoom');

});


$(document).on('closed', '.popup-img', function (e) {
    $('.js-img-zoom img').remove();

});


$('.js-img-zoom').panzoom({
    $zoomOut: $('.js-btn-zoom-out'),
    $reset: $('.js-btn-zoom-reset'),
    $zoomIn: $('.js-btn-zoom-in'),
    panOnlyWhenZoomed: true,
    minScale: 1,
});