$('.js-slider-main').owlCarousel({
    items: 1,
    loop: true,
    nav: false,
    dots: true,
    mouseDrag: false,
    autoplay: true,
    animateOut: 'fadeOut',
    animateIn: 'slideOutin',
    onInitialized: startProgressBarMain,
    onTranslate: resetProgressBarMain,
    onTranslated: startProgressBarMain
});

function startProgressBarMain(event) {

  $(".js-progress-bar-main").css({
    width: "100%",
    transition: "width 4000ms"
  });
  if (!event.namespace) {
      return;
    }
    var slides = event.relatedTarget;
    $('.main-slider__counter').html(slides.relative(slides.current()) + 1 + '<span>' + ' /' + slides.items().length + '</span>');
}

function resetProgressBarMain() {
  $(".js-progress-bar-main").css({
    width: 0,
    transition: "width 0s"
  });
}