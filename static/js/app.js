"use strict";

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/**
  * Возвращает HTML-код иконки из SVG-спрайта
  *
  * @param {String} name Название иконки из спрайта
  * @param {Object} opts Объект настроек для SVG-иконки
  *
  * @example SVG-иконка
  * getSVGSpriteIcon('some-icon', {
  *   tag: 'div',
  *   type: 'icons', // colored для подключения иконки из цветного спрайта
  *   class: '', // дополнительные классы для иконки
  *   mode: 'inline', // external для подключаемых спрайтов
  *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
  * });
  */
function getSVGSpriteIcon(name, opts) {
  opts = _extends({
    tag: 'div',
    type: 'icons',
    "class": '',
    mode: 'inline',
    url: ''
  }, opts);
  var external = '';
  var typeClass = '';

  if (opts.mode === 'external') {
    external = "".concat(opts.url, "/sprite.").concat(opts.type, ".svg");
  }

  if (opts.type !== 'icons') {
    typeClass = " svg-icon--".concat(opts.type);
  }

  opts["class"] = opts["class"] ? " ".concat(opts["class"]) : '';
  return "\n    <".concat(opts.tag, " class=\"svg-icon svg-icon--").concat(name).concat(typeClass).concat(opts["class"], "\" aria-hidden=\"true\" focusable=\"false\">\n      <svg class=\"svg-icon__link\">\n        <use xlink:href=\"").concat(external, "#").concat(name, "\"></use>\n      </svg>\n    </").concat(opts.tag, ">\n  ");
}
/* ^^^
 * JQUERY Actions
 * ========================================================================== */


$(function () {
  'use strict';
  /**
   * определение существования элемента на странице
   */

  $.exists = function (selector) {
    return $(selector).length > 0;
  };

  $('.js-slider-customers-info').owlCarousel({
    items: 1,
    loop: true,
    nav: true,
    dots: true,
    mouseDrag: false,
    animateOut: 'fadeOut',
    animateIn: 'slideOutin',
    navText: ['<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/></g></svg>', '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/></g></svg>']
  });
  $('.js-machinery-slider').owlCarousel({
    items: 3,
    loop: true,
    nav: true,
    dots: false,
    autoplay: true,
    margin: 20,
    onInitialized: startProgressBarMachinery,
    onTranslate: resetProgressBarMachinery,
    onTranslated: startProgressBarMachinery,
    responsive: {
      0: {
        items: 1
      },
      960: {
        items: 2
      },
      1280: {
        items: 3
      }
    },
    navText: ['<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/></g></svg>', '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/></g></svg>']
  });

  function startProgressBarMachinery(event) {
    $(".js-progress-bar-machinery").css({
      width: "100%",
      transition: "width 4000ms"
    });

    if (!event.namespace) {
      return;
    }

    var slides = event.relatedTarget;
    $('.machinery__counter').html(slides.relative(slides.current()) + 1 + '<span>' + ' /' + slides.items().length + '</span>');
  }

  function resetProgressBarMachinery() {
    $(".js-progress-bar-machinery").css({
      width: 0,
      transition: "width 0s"
    });
  }

  $('.js-slider-main').owlCarousel({
    items: 1,
    loop: true,
    nav: false,
    dots: true,
    mouseDrag: false,
    autoplay: true,
    animateOut: 'fadeOut',
    animateIn: 'slideOutin',
    onInitialized: startProgressBarMain,
    onTranslate: resetProgressBarMain,
    onTranslated: startProgressBarMain
  });

  function startProgressBarMain(event) {
    $(".js-progress-bar-main").css({
      width: "100%",
      transition: "width 4000ms"
    });

    if (!event.namespace) {
      return;
    }

    var slides = event.relatedTarget;
    $('.main-slider__counter').html(slides.relative(slides.current()) + 1 + '<span>' + ' /' + slides.items().length + '</span>');
  }

  function resetProgressBarMain() {
    $(".js-progress-bar-main").css({
      width: 0,
      transition: "width 0s"
    });
  }

  $('.js-btn-burger').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('btn-burger-is-active');
    $('.mobile-panel .menu-mob').toggleClass('menu-is-active');
    $('.mobile-panel__overlay-menu').toggleClass('overlay-is-active');
  });
  $.each($('.menu-mob .main-menu__nav ul').find('> li'), function (index, element) {
    if ($(element).find(' > ul').length) {
      var triggerIcon = ['<div class="svg-icon svg-icon--angle-down">', '</div>'].join('');
      var subMenuTrigger = $('<div class="sub-menu-trigger">' + triggerIcon + '</div>');
      $(element).addClass('haschild').append(subMenuTrigger);
    }
  });
  $('.menu-mob .main-menu__nav ul .sub-menu-trigger').on('click', function (event) {
    $(this).toggleClass('rotade');

    if (!$(this).closest('li').find('>ul').length) {
      return;
    }

    event.preventDefault();
    $(this).closest('li').toggleClass('open').find('>ul').stop().slideToggle();
  });

  function btnPhone() {
    $('.js-btn-phone').on('click', function () {
      $('.js-contacts-body').fadeToggle();
    });
    $(document).on('click', function (e) {
      if ($(e.target).closest('.mobile-panel').length) {
        return;
      }

      $('.js-contacts-body').fadeOut();
    });
  }

  btnPhone();
  $('.js-our-clients-slider').owlCarousel({
    items: 4,
    loop: true,
    nav: true,
    dots: false,
    responsive: {
      0: {
        items: 1
      },
      480: {
        items: 2
      },
      768: {
        items: 3
      },
      1024: {
        items: 4
      }
    },
    navText: ['<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/></g></svg>', '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/></g></svg>']
  });
  var PAGE = $('html, body');
  var pageScroller = $('.page-scroller');
  var inMemoryClass = 'page-scroller--memorized';
  var isVisibleClass = 'page-scroller--visible';
  var enabledOffset = 60;
  var pageYOffset = 0;
  var inMemory = false;

  function resetPageScroller() {
    setTimeout(function () {
      if (window.pageYOffset > enabledOffset) {
        pageScroller.addClass(isVisibleClass);
      } else if (!pageScroller.hasClass(inMemoryClass)) {
        pageScroller.removeClass(isVisibleClass);
      }
    }, 150);

    if (!inMemory) {
      pageYOffset = 0;
      pageScroller.removeClass(inMemoryClass);
    }

    inMemory = false;
  }

  if (pageScroller.length > 0) {
    window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
      passive: true
    } : false);
    pageScroller.on('click', function (event) {
      event.preventDefault();
      window.removeEventListener('scroll', resetPageScroller);

      if (window.pageYOffset > 0 && pageYOffset === 0) {
        inMemory = true;
        pageYOffset = window.pageYOffset;
        pageScroller.addClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: 0
        }, 500, 'swing', function () {
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      } else {
        pageScroller.removeClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: pageYOffset
        }, 500, 'swing', function () {
          pageYOffset = 0;
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      }
    });
  }

  $('.js-popup-form').on('submit', function (event) {
    event.preventDefault();
    $('.popup-form__notification').slideDown();
    $('.js-popup-form').slideUp();
  });
  $('.js-img-viewing').on('click', function (event) {
    event.preventDefault();
    var $this = $(this),
        imgSrc = $this.find('img').clone(),
        inRemodalImg = $this.closest('.app').find('.popup-img');
    imgSrc.appendTo('.js-img-zoom');
  });
  $(document).on('closed', '.popup-img', function (e) {
    $('.js-img-zoom img').remove();
  });
  $('.js-img-zoom').panzoom({
    $zoomOut: $('.js-btn-zoom-out'),
    $reset: $('.js-btn-zoom-reset'),
    $zoomIn: $('.js-btn-zoom-in'),
    panOnlyWhenZoomed: true,
    minScale: 1
  });
  $('.projects-tabs__items').on('click', '.js-tab-toggle:not(.tab-toggle-is-active)', function () {
    $(this).addClass('tab-toggle-is-active').siblings().removeClass('tab-toggle-is-active');
    $('.projects-tabs__main').find('.js-tab-content').removeClass('tab-content-is-active').hide().eq($(this).index()).fadeIn();

    if (window.matchMedia("(max-width: 1279px)").matches) {
      var scrTop = $(document).scrollTop(),
          offsetTop = $(this).offset().top + 300;

      if (scrTop < offsetTop) {
        $('html, body').animate({
          scrollTop: offsetTop
        }, 500);
      }
    }
  });
  $('.js-slider-to-content').owlCarousel({
    items: 1,
    loop: true,
    nav: true,
    dots: true,
    mouseDrag: false,
    animateOut: 'fadeOut',
    animateIn: 'slideOutin',
    navText: ['<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/></g></svg>', '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/></g></svg>']
  });
  var centerLocationMap = {
    lat: 55.807529,
    lng: 37.5683752
  };

  function locationMap() {
    var locationMapId = document.getElementById('location_map');

    if (!locationMapId) {
      return;
    }

    var map = new google.maps.Map(locationMapId, {
      center: {
        lat: centerLocationMap.lat,
        lng: centerLocationMap.lng
      },
      disableDefaultUI: true,
      zoom: 16
    });
    var marker = new google.maps.Marker({
      position: {
        lat: centerLocationMap.lat,
        lng: centerLocationMap.lng
      },
      map: map,
      icon: 'img/loc.svg'
    });
  }

  locationMap();

  if (window.matchMedia("(min-width: 1025px)").matches) {
    var fixedPanel = $('.js-fixed-panel');
    $(window).scroll(function () {
      if ($(this).scrollTop() > 150 && fixedPanel.hasClass('fixed-panel-init')) {
        fixedPanel.addClass('fixed-panel-show');
        $('.app-header').css('padding-top', '98px');
      } else if ($(this).scrollTop() <= 200 && fixedPanel.hasClass('fixed-panel-init')) {
        fixedPanel.removeClass('fixed-panel-show');
        $('.app-header').css('padding-top', '0');
      }
    });
  }
});